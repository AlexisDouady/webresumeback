<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PersonTableSeeder::class);
        $this->call(ResumeTableSeeder::class);
        $this->call(SkillCategoryTableSeeder::class);
        $this->call(SkillTableSeeder::class);
        $this->call(ExperienceSeeder::class);
    }
}
