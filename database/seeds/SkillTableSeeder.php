<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert([[
            'name' => 'HTML, CSS et Twig',
            'value' => 90,
            'skill_category_id' => 1
        ],[
            'name' => 'PHP orienté objet, Laravel et Symfony',
            'value' => 75,
            'skill_category_id' => 1
        ],[
            'name' => 'Javascript et JQuery',
            'value' => 60,
            'skill_category_id' => 1
        ],[
            'name' => 'Maquettage graphique',
            'value' => 60,
            'skill_category_id' => 1
        ],[
            'name' => 'Langage C',
            'value' => 75,
            'skill_category_id' => 2
        ],[
            'name' => 'Java',
            'value' => 75,
            'skill_category_id' => 2
        ],[
            'name' => 'MySQL',
            'value' => 75,
            'skill_category_id' => 2
        ],[
            'name' => 'Git',
            'value' => 50,
            'skill_category_id' => 2
        ],[
            'name' => 'Travail d\'équipe',
            'value' => 75,
            'skill_category_id' => 3
        ],[
            'name' => 'Pédagogie',
            'value' => 75,
            'skill_category_id' => 3
        ],[
            'name' => 'Rapidité d\'apprentissage',
            'value' => 75,
            'skill_category_id' => 3
        ],[
            'name' => 'Linux et Shell',
            'value' => 50,
            'skill_category_id' => 4
        ],[
            'name' => 'Réseaux Cisco et Cisco IOS',
            'value' => 30,
            'skill_category_id' => 4
        ]]);
    }
}
