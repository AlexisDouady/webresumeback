<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skill_categories')->insert([[
            'title' => 'Création et Développement Web',
            'resume_id' => 1
        ],[
            'title' => 'Développement et Génie Logiciel',
            'resume_id' => 1
        ],[
            'title' => 'Soft Skills',
            'resume_id' => 1
        ],[
            'title' => 'Systèmes et Réseaux',
            'resume_id' => 1
        ]]);
    }
}
