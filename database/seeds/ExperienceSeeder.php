<?php

use Illuminate\Database\Seeder;

class ExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experiences')->insert([
            [
                'beginning' => \Illuminate\Support\Facades\Date::create(2018, 1, 1)->year,
                'end' => \Illuminate\Support\Facades\Date::create(2023, 1, 1)->year,
                'title' => 'CESI Ecole d\'Ingénieurs',
                'subtitle' => 'Deuxième année, spécialité Informatique',
                'description' =>
                    '- Apprentissage par problèmes et par projets (PBL)
                \n- Réunions quatre fois par semaine
                \n- Etude de la programmation, du réseau et des bases de l\'électronique
                \n- Ecole reconnue par la Commission des Titres d\'Ingénieurs (CTI)',
                'resume_id' => 1,
                'is_school' => true
            ],
            [
                'beginning' => \Illuminate\Support\Facades\Date::create(2015, 1, 1)->year,
                'end' => \Illuminate\Support\Facades\Date::create(2018, 1, 1)->year,
                'title' => 'Baccalauréat Scientifique',
                'subtitle' => 'Mention Très Bien, Lycée Honoré d\'Estienne d\'Orves',
                'description' =>
                    '- 17.07 de moyenne au Baccalauréat
                    \n- 20/20 de moyenne en Informatique et Sciences du Numérique
                    \n- Classe européenne Espagnole de la seconde à la première',
                'resume_id' => 1,
                'is_school' => true
            ],
            [
                'beginning' => \Illuminate\Support\Facades\Date::create(2017, 1, 1)->year,
                'end' => \Illuminate\Support\Facades\Date::create(2017, 1, 1)->year,
                'title' => 'Certification B1 en langue Espagnole',
                'subtitle' => 'Académie de Cervantes',
                'description' =>
                    '- Ecrit
                    \n- Lu
                    \n- Parlé',
                'resume_id' => 1,
                'is_school' => true
            ],
            [
                'beginning' => \Illuminate\Support\Facades\Date::create(2011, 1, 1)->year,
                'end' => \Illuminate\Support\Facades\Date::create(2015, 1, 1)->year,
                'title' => 'Diplôme National du Brevet',
                'subtitle' => 'Mention Très Bien, Collège Alphonse Daudet (Nice)',
                'description' =>
                    '- 16.01 de moyenne au Brevet
                    \n- Classe européenne Espagnole de la quatrième à la troisième',
                'resume_id' => 1,
                'is_school' => true
            ],
            [
                'beginning' => \Illuminate\Support\Facades\Date::create(2018, 07, 14)->year,
                'end' => \Illuminate\Support\Facades\Date::create(2019, 07, 01)->year,
                'title' => 'Vendeur traiteur',
                'subtitle' => 'Rôtisserie du palais, Nice',
                'description' =>
                    '- Travail d\'équipe
                    \n- Vente des différents produits traiteur
                    \n- Relationnel client
                    \n- Gestion de la caisse',
                'resume_id' => 1,
                'is_school' => false
            ],
            [
                'beginning' => \Illuminate\Support\Facades\Date::create(2014, 07, 14)->year,
                'end' => \Illuminate\Support\Facades\Date::create(2014, 07, 01)->year,
                'title' => 'Stagiaire d\'observation',
                'subtitle' => 'SARL Enseignes Bertrand',
                'description' =>
                    '- Découverte du monde du travail
                    \n- Apprentissage des logiciels Photoshop et Illustrator
                    \n- Apprentissage du processus de création d\'une enseigne',
                'resume_id' => 1,
                'is_school' => false
            ]
        ]);
    }
}
