<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class PersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('persons')->insert([
            'name'      => 'Alexis',
            'last_name' => 'DOUADY',
            'status'    => 'Etudiant en Ingénierie Informatique',
            'birth'     => Date::createFromDate(2000, 15, 11),
            'city'      => 'Sophia Antipolis',
            'email'     => 'alexis.douady@viacesi.fr',
            'phone'     => '06 58 80 31 00',
            'linkedin'  => 'https://www.linkedin.com/in/alexis-douady/',
            'bitbucket' => 'https://bitbucket.org/AlexisDouady/profile/repositories'
        ]);
    }
}
