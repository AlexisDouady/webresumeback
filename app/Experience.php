<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experiences';

    function resume()
    {
        return $this->belongsTo('App\Resume');
    }
}
