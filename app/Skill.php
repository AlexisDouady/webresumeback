<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table = 'skills';

    function category()
    {
        return $this->belongsTo('App\SkillCategory');
    }
}
