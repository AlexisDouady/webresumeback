<?php

namespace App\Repository;

interface RepositoryInterface
{
    function getPaginate($n);
    function store(Array $inputs);
    function getById($id);
    function update($id, Array $inputs);
    function destroy($îd);
}
