<?php


namespace App\Repositories;


use App\Repository\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements RepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    function getPaginate($n)
    {
        return $this->model->paginate($n);
    }

    function store(array $inputs)
    {
        return $this->model->create($inputs);
    }

    function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    function update($id, array $inputs)
    {
        $this->getById($id)->update($inputs);
    }

    function destroy($îd)
    {
        $this->getById($id)->delete();
    }

}
