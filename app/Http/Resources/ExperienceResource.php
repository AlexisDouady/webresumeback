<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        [
            'beginning' => $this->beginning,
            'end' => $this->end,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'description' => $this->description
        ];
    }
}
