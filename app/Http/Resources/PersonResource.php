<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        [
          'id' => $this->id,
          'name' => $this->name,
          'last_name' => $this->last_name,
          'status' => $this->status,
          'birth' => $this->birth,
          'city' => $this->city,
          'email' => $this->email,
          'phone' => $this->phone,
          'linkedin' => $this->linkedin,
          'bitbucket' => $this->bitbucket
        ];
    }
}
