<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';

    function resumes()
    {
        return $this->hasMany('App\Resume');
    }
}
