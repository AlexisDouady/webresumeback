<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkillCategory extends Model
{
    protected $table = 'skill_categories';

    function skills()
    {
        return $this->hasMany('App\Skill');
    }

    function resume()
    {
        return $this->belongsTo('App\Resume');
    }
}
