<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $table = 'resumes';

    function person()
    {
        return $this->belongsTo('App\Person');
    }

    function skillCategories()
    {
        return $this->hasMany('App\SkillCategory');
    }

    function experiences()
    {
        return $this->hasMany('App\Experience');
    }

    function schools()
    {
        return $this->experiences()->where('is_school', '=', 1);
    }

    function works()
    {
        return $this->experiences()->where('is_school', '=', 0);
    }
}
